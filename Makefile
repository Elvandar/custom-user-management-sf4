MYSQL_PASSWORD = $(shell tr -dc "a-zA-Z0-9_+-" < /dev/urandom | head -c "16")
UID := $(shell id -u)
GID := $(shell id -g)
STACK = $(notdir $(CURDIR))
NETWORK = $(STACK)_default
DC = docker-compose
D = docker

# Colors
_END = \033[0m
_CYAN = \033[36m

.PHONY: help all build rebuild start stop restart clean cache-clear composer-install database-install adminer pma php logs

help: ## Print all commands (default)
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

all: build start ## Build project then start it


## ---- SPECIFIC SYMFONY INSTALLATION TARGETS

composer-install: ## Install back-end dependencies (composer)
	@echo
	@echo "========================"
	@echo "$(_CYAN)COMPOSER install start !$(_END)"
	@echo "========================"
	@echo
	$(DC) run --no-deps --rm -u $(UID):$(GID) symfony \
		composer install --ignore-platform-reqs --no-scripts

database-install: ## Create and setup the database
	$(DC) up -d db
	$(DC) run --no-deps --rm symfony \
		sh -c './bin/console doctrine:database:create --if-not-exists && ./bin/console doctrine:schema:update --force'

cache-clear: ## clear symfony cache
	$(DC) run --no-deps --rm -u $(UID):$(GID) symfony \
		sh -c './bin/console cache:clear'


## ---- DATABASE TOOLS TARGETS

adminer: ##@database Launch adminer on port 8080 linked to current app network (host = db)
	$(D) run --rm --net $(NETWORK) -p 8080:8080 --name adminer adminer

pma: ##@database Launch phpmyadmin on port 8080 linked to current app network (host = db)
	$(D) run --rm --net $(NETWORK) -p 8080:80 --name pma phpmyadmin/phpmyadmin


## ---- MAIN TARGETS

docker-compose.override.yml:
	@sed -e "s/{MYSQL_PASSWORD}/$(MYSQL_PASSWORD)/g" \
	     -e "s/{UID}/$(UID)/g" \
	     -e "s/{GID}/$(GID)/g" \
		docker-compose.override.sample.yml > docker-compose.override.yml

build: docker-compose.override.yml ## Build project
	@echo
	@echo "============================"
	@echo "$(_CYAN)docker-compose build start !$(_END)"
	@echo "============================"
	@echo
	$(DC) build

rebuild: docker-compose.override.yml ## Rebuild project (build --no-cache --force-rm)
	$(DC) build --no-cache --force-rm

start: composer-install cache-clear ## Start all containers (start)
	@echo
	@echo "============================"
	@echo "$(_CYAN)docker-compose up -d start !$(_END)"
	@echo "============================"
	@echo
	$(DC) up -d
	@echo
	@echo "============================="
	@echo "$(_CYAN)You have to build front-end !$(_END)"
	@echo "============================="
	@echo
	@echo "$(_CYAN)To achieve this, please run one of these following commands:$(_END)"
	@echo "$(_CYAN)1) make front-dev   # compile assets once$(_END)"
	@echo "$(_CYAN)2) make front-watch # recompile assets automatically when files change$(_END)"
	@echo "$(_CYAN)3) make front-prod  # compile assets, but also minify & optimize then$(_END)"
	@echo
	@echo "=============================="
	@echo "$(_CYAN)You have to install database !$(_END)"
	@echo "=============================="
	@echo 
	@echo "$(_CYAN)To achieve this, please run the following command:$(_END)"
	@echo "$(_CYAN)make database-install$(_END)"

stop: ## Stop all containers (stop)
	$(DC) stop

restart: stop start ## Restart all containers (stop start)

clean: ## Clean all volumes (down -v) and delete generated and downloaded files
	$(DC) down -v --remove-orphans --rmi local
	rm -rf $(CURDIR)/docker-compose.override.yml
	rm -rf $(CURDIR)/www/vendor
	rm -rf $(CURDIR)/www/var
	rm -rf $(CURDIR)/www/node_modules
	rm -rf $(CURDIR)/www/public/build


## ---- SHORTCUTS TO ENTER WITHIN CONTAINERS

symfony: ## Open bash session in symfony container as host user
	$(DC) run --rm -u $(UID):$(GID) symfony sh

logs: ## Display the logs of all containers
	$(DC) logs

