<?php

namespace App\Model;


use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class User
{
    /**
     * @var string
     * @Assert\Email(message="Veuillez renseigner un e-mail valide.")
     */
    public $email;

    /**
     * @var string
     * @Assert\Length(max=250)
     * @Assert\NotNull(message="Veuillez renseigner un mot de passe.")
     */
    public $password;
}
