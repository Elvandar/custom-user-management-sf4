<?php

namespace App\Form\Handler;


use App\Entity\User;
use App\Model\User as UserModel;
use App\Events;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class RegistrationHandler
{
    /**
     * @var ObjectManager
     *
     * Allows to save user in database
     */
    private $objectManager;

    /**
     * @var LoggerInterface
     *
     * Allows to give errors found within the logs file
     */
    private $loggerInterface;

    public function __construct(ObjectManager $objectManager, LoggerInterface $loggerInterface)
    {
        $this->objectManager = $objectManager;
        $this->loggerInterface = $loggerInterface;
    }

    public function handle(FormInterface $form, Request $request, UserPasswordEncoderInterface $passwordEncoder, EventDispatcherInterface $eventDispatcher)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var UserModel $userModel
             */
            $userModel = $form->getData();

            /**
             * @var User $user
             */
            $user = new User();

            $password = $passwordEncoder->encodePassword($user, $userModel->password);
            $user->setPassword($password);
            $user->setEmail($userModel->email);
            $user->setIsActive(true);
            //$user->addRole("ROLE_ADMIN");

            try {
                $this->objectManager->persist($user);
            } catch (ORMException $e) {
                $this->loggerInterface->error($e->getMessage());
                $form->addError(new FormError('Erreur lors de l\'insertion en base du user...'));
                return false;
            }

            $this->objectManager->flush();

            // sending an email for each new user created
            $event = new GenericEvent($user);
            $eventDispatcher->dispatch(Events::USER_REGISTERED, $event);

            return true;
        }

        return false;
    }
}
