<?php

namespace App\Form\Handler;


use App\Entity\User;
use App\Events;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class ResettingHandler
{
    /**
     * @var ObjectManager
     *
     * Allows to save user in database
     */
    private $objectManager;

    /**
     * @var LoggerInterface
     *
     * Allows to give errors found within the logs file
     */
    private $loggerInterface;

    public function __construct(ObjectManager $objectManager, LoggerInterface $loggerInterface)
    {
        $this->objectManager = $objectManager;
        $this->loggerInterface = $loggerInterface;
    }

    public function handleRequest(FormInterface $form, Request $request, EventDispatcherInterface $eventDispatcher)
    {
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            /**
             * @var User $user
             */
            $user = $this->objectManager
                ->getRepository(User::class)
                ->findOneByEmail($form->getData()['_email']);

            if ($user !== null) {
                $token = uniqid();
                $user->setResetPasswordToken($token);

                try {
                    $this->objectManager->persist($user);
                } catch (ORMException $e) {
                    $this->loggerInterface->error($e->getMessage());
                    $form->addError(new FormError('Erreur lors de la réinitialisation du mot de passe en base du user...'));
                    return false;
                }

                $this->objectManager->flush();

                // sending email
                $event = new GenericEvent($user);
                $eventDispatcher->dispatch(Events::USER_RESETTING, $event);

                return true;                
            }
        }

        return false;
    }

    public function handleReset(FormInterface $form, Request $request, UserPasswordEncoderInterface $passwordEncoder, $token)
    {
        /**
         * @var User $user
         */
        $user = $this->objectManager
            ->getRepository(User::class)
            ->findOneByResetPasswordToken($token);

        if ($user !== null) {

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {
                $plainPassword = $form->getData()['_new_password'];
                $encoded = $passwordEncoder->encodePassword($user, $plainPassword);

                $user->setPassword($encoded);

                try {
                    $this->objectManager->persist($user);
                } catch (ORMException $e) {
                    $this->loggerInterface->error($e->getMessage());
                    $form->addError(new FormError('Erreur lors de la réinitialisation du mot de passe en base du user...'));
                    return false;
                }

                $this->objectManager->flush();

                return true; 
            }
        }

        return false;
    }
}
