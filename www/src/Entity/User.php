<?php 

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity(fields="email", message="Email déjà pris")
 * 
 * Defines the properties of the User entity to represent the application users.
 * See https://symfony.com/doc/current/book/doctrine.html#creating-an-entity-class
 * 
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class User implements UserInterface, \Serializable {

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(name="resetPasswordToken", type="string", length=255, nullable=true)
     */
    protected $resetPasswordToken;

    /**
     * The below length depends on the "algorithm" you use for encoding
     * the password, but this works well with bcrypt.
     *
     * @ORM\Column(name="password", type="string", length=64)
     */
    protected $password;

    /**
     * @var boolean
     * 
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive;

    /**
     * @ORM\Column(name="roles", type="array")
     */
    private $roles = array();

    public function __construct() 
    {
        $this->isActive = true;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string 
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getResetPasswordToken(): ?string
    {
        return $this->resetPasswordToken;
    }

    /**
     * @param string $resetPasswordToken
     * @return User
     */
    public function setResetPasswordToken(string $resetPasswordToken): User
    {
        $this->resetPasswordToken = $resetPasswordToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsActive(): bool 
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive 
     * @return User
     */
    public function setIsActive(bool $isActive): User 
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Return user roles
     * @return array
     */
    public function getRoles(): array 
    {
        if (empty($this->roles)) {
            return ['ROLE_USER'];
        }
        return $this->roles;
    }

    function addRole($role) 
    {
        $this->roles[] = $role;
    }

    /**
     * Return salt
     *
     * {@inheritdoc}
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials(): void 
    {
        
    }

    /**
     * {@inheritdoc}
     *
     * @see \Serializable::serialize()
     */
    public function serialize(): string 
    {
        return serialize([
            $this->id,
            $this->email,
            $this->password,
            $this->isActive
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized) 
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            $this->isActive
        ) = unserialize($serialized);
    }
}