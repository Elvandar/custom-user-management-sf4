<?php

namespace App\EventSubscriber;


use App\Entity\User;
use App\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Send a reset password email confirmation
 *
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class ResettingNotifySubscriber implements EventSubscriberInterface
{   
    private $mailer;
    private $twig;
    private $sender;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, $sender)
    {
        $this->mailer = $mailer;
        $this->twig   = $twig;
        $this->sender = $sender;        
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::USER_RESETTING => 'onUserResetting',
        ];
    }

    public function onUserResetting(GenericEvent $event): void
    {
        /** @var User $user */
        $user = $event->getSubject();

        $subject = "Mot de passe oublié ?";
        $body = $this->renderTemplate($user->getResetPasswordToken());

        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setTo($user->getEmail())
            ->setFrom($this->sender)
            ->setBody($body, 'text/html')
        ;

        $this->mailer->send($message);
    }

    private function renderTemplate($token)
    {
        return $this->twig->render(
            'emails/resetting.html.twig',
            ['token' => $token]
        );
    }
}