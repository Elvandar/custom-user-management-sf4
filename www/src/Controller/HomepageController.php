<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class HomepageController extends Controller 
{
    /**
     * @Route("/")
     */
    public function index() 
    {
        return $this->render('homepage/index.html.twig', [
        	'mainNavHome'=>true, 
        	'title'=>'Accueil'
        ]);
    }

}