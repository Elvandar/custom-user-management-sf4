<?php 

namespace App\Controller;


use App\Entity\User;
use App\Form\Handler\ResettingHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class ResettingController extends Controller 
{
    /**
     * @Route("/resetting-request", name="app_resetting_request")
     */
    public function resetRequest(ResettingHandler $formHandler, Request $request, EventDispatcherInterface $eventDispatcher)
    {
    	$form = $this->get('form.factory')
            ->createNamedBuilder(null)
            ->add('_email', \Symfony\Component\Form\Extension\Core\Type\EmailType::class, [
                'label' => 'E-mail'
            ])
            ->add('ok', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, [
                'label' => 'Ok', 
                'attr'  => ['class' => 'btn-primary btn-block']
            ])
            ->getForm();

        if ($formHandler->handleRequest($form, $request, $eventDispatcher)) {
            return $this->render('resetting/reset-password-confirmation.html.twig');
        }

        return $this->render('resetting/reset-password.html.twig', [
        	'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/resetting-reset/{token}", name="app_resetting_reset")
     */
    public function reset(ResettingHandler $formHandler, Request $request, UserPasswordEncoderInterface $encoder, $token)
    {
        if ($token !== null) {

            $form = $this->get('form.factory')
                ->createNamedBuilder(null)
                ->add('_new_password', \Symfony\Component\Form\Extension\Core\Type\PasswordType::class, [
                    'label' => 'Nouveau mot de passe'
                ])
                ->add('ok', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, [
                    'label' => 'Modifier mon mot de passe', 
                    'attr'  => ['class' => 'btn-primary btn-block']
                ])
                ->getForm();

            if ($formHandler->handleReset($form, $request, $encoder, $token)) {
                $this->addFlash('success', 'Votre mot de passe a bien été modifié.');
                return $this->redirectToRoute('app_login');
            }
            
            return $this->render('resetting/reset-password-token.html.twig', [
                'form' => $form->createView(),
            ]); 
        }
    }

}