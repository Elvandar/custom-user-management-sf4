<?php 

namespace App\Controller;

use App\Form\Type\UserType;
use App\Form\Handler\RegistrationHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class RegistrationController extends Controller 
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(RegistrationHandler $formHandler, Request $request, UserPasswordEncoderInterface $passwordEncoder, EventDispatcherInterface $eventDispatcher)
    {
        $form = $this->createForm(UserType::class);

        if ($formHandler->handle($form, $request, $passwordEncoder, $eventDispatcher)) {
            $this->addFlash('success', 'Votre compte a bien été enregistré.');
            // return $this->redirectToRoute('app_login');
        }

        return $this->render('registration/register.html.twig', [
            'form' => $form->createView(), 
            'mainNavRegistration' => true, 
            'title' => 'Inscription'
        ]);
    }

}