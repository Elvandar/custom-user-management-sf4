<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class SecurityController extends Controller 
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils) 
    {
        $form = $this->get('form.factory')
            ->createNamedBuilder(null)
            ->add('_username', null, [
                'label' => 'Email'
            ])
            ->add('_password', \Symfony\Component\Form\Extension\Core\Type\PasswordType::class, [
                'label' => 'Mot de passe'
            ])
            ->add('ok', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, [
                'label' => 'Se connecter', 
                'attr'  => ['class' => 'btn-primary btn-block']
            ])
            ->getForm();

        return $this->render('security/login.html.twig', [
            'mainNavLogin' => true, 
            'title' => 'Connexion',
            'form'  => $form->createView(),
            'error' => $authenticationUtils->getLastAuthenticationError()
        ]);
    }

}