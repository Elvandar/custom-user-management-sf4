<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/member")
 * 
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class MemberController extends Controller 
{
    /**
     * @Route("/")
     */
    public function index()
    {
        return $this->render('member/index.html.twig', [
        	'mainNavMember'=>true, 
        	'title'=>'Espace Membre'
        ]);
    }

}