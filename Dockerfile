FROM php:7.2-fpm-alpine

ENV APP_DIR /var/www/html 

COPY www "$APP_DIR/"
WORKDIR "$APP_DIR/"

# Install Composer
ENV COMPOSER_ALLOW_SUPERUSER 1

RUN php -r "readfile('https://getcomposer.org/installer');" | php -- --install-dir=/usr/local/bin --filename=composer \
    && chmod +x /usr/local/bin/composer \
    # See https://github.com/hirak/prestissimo
    && composer global require "hirak/prestissimo:^0.3"

# Install necessary PHP extensions
RUN docker-php-ext-install \
	pdo_mysql 

# Install back-end dependencies
RUN composer install --no-dev --no-plugins --no-scripts

# allow overwriting UID and GID o the user "www-data" to help solve issues with permissions in mounted volumes
# if the GID is already in use, we will assign GID 82 instead (82 is the standard uid/gid for "www-data" in Alpine)
ARG www_data_uid
ARG www_data_gid

RUN deluser www-data \
    && (addgroup -g $www_data_gid www-data || addgroup -g 82 www-data) \
    && adduser -u $www_data_uid -D -S -G www-data www-data \
    && chown -R www-data:www-data "$APP_DIR/"

USER www-data

CMD ["php-fpm"]
