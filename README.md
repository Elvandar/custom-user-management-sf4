# Docker-Symfony starter pack

This is a starter skeleton based on a Symfony4 framework by using [Docker](https://www.docker.com/) & [Docker-Compose](https://docs.docker.com/compose/).

We will use 3 services :

- a web service : **apache2** (using a custom apache2 image in order to install our own tools)
- a php service : **php-fpm** (using an existant docker image and adaptation of the Dockerfile)
- a database service : **mariadb** (using an existant image based on mariadb:10.3)

## Summary

1. [Requirements](#requirements)
2. [Installation](#installation)
3. [Usage](#usage)

## Requirements

- [GIT](https://installations.delicious-insights.com/software/git.html)
- [Docker](https://www.docker.com/)
- [Docker-Compose](https://docs.docker.com/compose/)

If you don't have installed these tools in your local system, please checks out this following [documentation](https://gitlab.byperiscope.com/periscope/prod).

## Installation

``` bash
$ git clone git@gitlab.byperiscope.com:periscope/starter/symfony.git
$ cd symfony
$ git checkout develop
```

## Usage

In a « development mode », we will use a Makefile in order to facilitate the installation of the application. To achieve this, stay in the root project and use the `make` commands line that you want to use. This is the main commands line to know :

<table>
    <tr>
        <td><strong>Commande</strong></td>
        <td><strong>Description</strong></td>
    </tr>
    <tr>
        <td>make</td>
        <td>Afficher toutes les commandes disponibles.</td>
    </tr>
    <tr>
        <td>make all</td>
        <td>Installer toutes les dépendances, compiler le code, générer les variables d'environnement et construire les containers docker.</td>
    </tr>
    <tr>
        <td>make clean</td>
        <td>Nettoyer les volumes et supprimer les fichiers générés dynamiquement.</td>
    </tr>
    <tr>
        <td>make symfony</td>
        <td>Entrer dans l'image symfony (pour accéder aux sources).</td>
    </tr>
    <tr>
        <td>make adminer</td>
        <td>Utiliser adminer.</td>
    </tr>
    <tr>
        <td>make pma</td>
        <td>Utiliser phpMyAdmin.</td>
    </tr>
</table>

> Info: make sure your locally installed apache server on your machine is not running or another instance which can use the port **80**.

After launching the `make all` command, go to [localhost](http://localhost/) in your browser to see Symfony starter UI.